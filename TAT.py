#!/usr/bin/env python
# coding: utf-8

import copy
import random
import math
import csv

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import scipy.cluster.hierarchy as spc
import multimatch_gaze as mmg

from mpl_toolkits.mplot3d import Axes3D
from wordcloud import WordCloud


# attention map
def attention_map(dataframe, locations, name,  map_file, width, height, line_color = '#6848c0', cluster_color = '#59c963', export = '', print_cluster = False):
    """
    Creates an attention map visualization based on the dataframe data.

    :param dataframe: dataframe with columns providing location information per second
    :param locations: dict of '<location name>': [x,y]
    :param name: title of the map as string
    :param map_file: path to mapfile for background map
    :param width: width of map in px
    :param height: height of map in px
    :param line_color: color of the paths (default = '#6848c0')
    :param cluster_color: color of the cluster points (default = '#59c963')
    :param export: path + filename to export map (default = '' // no map export)
    :param print_cluster: if True, prints cluster and path frequencies (default = False)
    :returns: void
    :raises keyError: raises an exception
    """
    
    # defining variables from input
    last_second = int(dataframe.iloc[:,-1].name)
    
    cluster_list = []
    cluster = {}
    for key in locations:
        cluster_list.append(int(key))
        cluster[key] = 0
    
    cluster_flow = {}
    for key in cluster:
        cluster_flow[key] = copy.deepcopy(cluster)

    # counting paths
    for index, row in dataframe.iterrows():
        before = '0'
        for i in range(1, last_second + 1):
            if (str(row[i]) in cluster):
                cluster[str(row[i])] = cluster[str(row[i])] +1
                if (before in locations) and (str(row[i]) != before):
                    cluster_flow[before][str(row[i])] = cluster_flow[before][str(row[i])] +1
            before = str(row[i])
    
    # printing frequencies
    if print_cluster == True:
        print("Cluster frequency: {}".format(cluster))
        print("Path frequency: {}".format(cluster_flow))
    
    # visualize path frequencies
    fig, ax = plt.subplots()
    img = plt.imread(map_file)

    for l in cluster_list:
        ax.scatter(locations[str(l)][0], locations[str(l)][1], c=cluster_color, s=cluster[str(l)]*2, zorder=1 )

    ax.imshow(img, zorder=0, extent=[0.0, width, 0.0, height])

    for j in cluster_list:
        for i in cluster_list:
            if cluster_flow[str(j)][str(i)] != 0:
                ax.arrow(locations[str(j)][0], locations[str(j)][1], 
                          (locations[str(i)][0]-locations[str(j)][0])*0.97,
                          locations[str(i)][1]-locations[str(j)][1], 
                      width = cluster_flow[str(j)][str(i)], 
                      shape = 'left', 
                      length_includes_head = True,
                      head_length = cluster_flow[str(j)][str(i)]*3,
                      color = line_color
                     )
                
    ax.set_xticks([])
    ax.set_yticks([])
    
    ax.set_title(name, fontsize = 20)
    fig.set_figheight(8) 
    fig.set_figwidth(12) 

    if export != '':
        fig.savefig(export+".png")

    plt.show()


# Attention map differences
def attention_map_differences(dataframe1, name1, dataframe2, name2, locations, map_file, width, height, export = ''):
    """
    Creates an attention map of differences visualization based on the two input dataframes

    :param dataframe1: dataframe with columns providing location information per second
    :param name1: name of dataframe1 as string
    :param dataframe2: dataframe to compare with
    :param name2: name of dataframe2 as string
    :param locations: dict of '<location name>': [x,y]
    :param map_file: path to mapfile for background map
    :param width: width of map in px
    :param height: height of map in px
    :param export: path + filename to export map (default = '' // no map export)
    :returns: void
    :raises keyError: raises an exception
    """

    # defining variables from input
    last_second = int(dataframe1.iloc[:,-1].name)
    proportion = len(dataframe1) / len(dataframe2)
    
    cluster_list = []
    cluster = {}
    for key in locations:
        cluster_list.append(int(key))
        cluster[key] = 0
    
    cluster_flow1 = {}
    cluster_flow2 = {}
    for key in cluster:
        cluster_flow1[key] = copy.deepcopy(cluster)
        cluster_flow2[key] = copy.deepcopy(cluster)

    for index, row in dataframe1.iterrows():
        before = '0'
        for i in range(1, last_second + 1):
            if (str(row[i]) in cluster):
                if (before in locations) and (str(row[i]) != before):
                    cluster_flow1[before][str(row[i])] = cluster_flow1[before][str(row[i])] +1
            before = str(row[i])
         #print(cluster_flow1)
        
    for index, row in dataframe2.iterrows():
        before = '0'
        for i in range(1, last_second + 1):
            if (str(row[i]) in cluster):
                if (before in locations) and (str(row[i]) != before):
                    cluster_flow2[before][str(row[i])] = cluster_flow2[before][str(row[i])] +1
            before = str(row[i])
         #print(cluster_flow2)
        
    
    fig, ax = plt.subplots()
    img = plt.imread(map_file)

    for l in cluster_list:
        plt.scatter(locations[str(l)][0], locations[str(l)][1], c='black', s=10, zorder=1 )

    plt.imshow(img, zorder=0, extent=[0.0, width, 0.0, height], alpha = 0.3)

    cmap = plt.cm.seismic

    cNorm  = colors.Normalize(vmin=-len(dataframe1), vmax= len(dataframe1))
    scalarMap = cmx.ScalarMappable(norm=cNorm,cmap=cmap)
    
    for j in cluster_list:
        for i in cluster_list:
            flow_sums = cluster_flow1[str(j)][str(i)] + cluster_flow2[str(j)][str(i)] 
            if flow_sums != 0:
                
                colorVal = scalarMap.to_rgba((cluster_flow1[str(j)][str(i)])-(cluster_flow2[str(j)][str(i)]*proportion))
                plt.arrow(locations[str(j)][0], locations[str(j)][1], 
                          locations[str(i)][0]-locations[str(j)][0],
                          locations[str(i)][1]-locations[str(j)][1], 
                      width = flow_sums , 
                      shape = 'left', 
                      length_includes_head = True,
                      head_length = flow_sums*3,
                      color = colorVal
                     )

    fig.set_figheight(12) 
    fig.set_figwidth(12)
    
    ax.annotate('more path from ' + name1,
            xy=(1, 0), xycoords='axes fraction',
            xytext=(-10, -75), textcoords='offset pixels',
            horizontalalignment='right',
            verticalalignment='bottom')
    
    ax.annotate('more path from ' + name2,
            xy=(1, 0), xycoords='axes fraction',
            xytext=(-500, -75), textcoords='offset pixels',
            horizontalalignment='right',
            verticalalignment='bottom')
    ax.set_axis_off()
    
    plt.colorbar(scalarMap, orientation="horizontal")
    
    if export != '':
        fig.savefig(export+".png")
        
    plt.show()


# Paths / Trajectory map
def trajectory_maps(dataframe, locations, map_file, width, height, line_color = '#6848c0', im_alpha = 0.7, export = '' ):
    """
    Creates an trajectory map visualizations for every row of the dataframe.

    :param dataframe: dataframe with columns providing location information per second
    :param locations: dict of '<location name>': [x,y]
    :param map_file: path to mapfile for background map
    :param width: width of map in px
    :param height: height of map in px
    :param line_color: color of the paths (default = '#6848c0')
    :param im_alpha: alpha value of background map as float (default = 0.7)
    :param export: path + filename to export map (default = '' // no map export)
    :returns: void
    :raises keyError: raises an exception
    """
    
    l= len(dataframe)
    last_second = int(dataframe.iloc[:,-1].name)
    
    img = plt.imread(map_file)
    fig_columns = 3
    if int(l%3) == 0: 
        fig_rows = int(l/3)
    else:
        fig_rows = int(l/3)+1
    
    fig, axs = plt.subplots(fig_rows, fig_columns, constrained_layout=True)
    indec = 0    
    for index, row in dataframe.iterrows():
        rown = int(int(indec)/3)
        coln = (int(indec)%3)

        start = 0
        ii = 1
        while (ii < (last_second +1) and (str(row[ii]) not in locations)):
            start+=1
            ii+=1
        x = []
        y = []
        before = '0'
        for i in range(start+1, last_second+1):
            if (str(row[i]) in locations) and (str(row[i]) != before):
                x.append(locations[str(row[i])][0]+random.uniform(-7.5, 7.5)) 
                y.append(locations[str(row[i])][1]+random.uniform(-7.5, 7.5))
                before = str(row[i])
        if fig_rows == 1:
            axs[coln].plot(x,y, c = line_color)

            axs[coln].imshow(img, zorder=0, extent=[0.0, width, 0.0, height], alpha = im_alpha)
            axs[coln].set_title(str(row['id']), fontsize= 40)
            axs[coln].set_xticks([])
            axs[coln].set_yticks([])
            
        else:
            axs[rown, coln].plot(x,y, c = line_color)

            axs[rown, coln].imshow(img, zorder=0, extent=[0.0, width, 0.0, height], alpha = im_alpha)
            axs[rown, coln].set_title(str(row['id']), fontsize= 40)
            axs[rown, coln].set_xticks([])
            axs[rown, coln].set_yticks([])
        indec +=1
            
    while indec < fig_rows * fig_columns:
        rown = int(int(indec)/3)
        coln = (int(indec)%3)
        axs[rown, coln].imshow(img, zorder=0, extent=[0.0, width, 0.0, height], alpha = 0.25)
        indec +=1

    fig.set_figheight(fig_rows*10) 
    fig.set_figwidth(40)
    
    if export != '':
        fig.savefig(export+".png")
        
    plt.show()


# Flowmap
def flow_map(dataframe, locations, map_file, width, height, line_color = '#6848c0', im_alpha = 0.8, export = '' ):
    """
    Creates an flow map visualization based on the input dataframe.

    :param dataframe: dataframe with columns providing location information per second
    :param locations: dict of '<location name>': [x,y]
    :param map_file: path to mapfile for background map
    :param width: width of map in px
    :param height: height of map in px
    :param line_color: color of the paths (default = '#6848c0')
    :param im_alpha: alpha value of background map as float (default = 0.8)
    :param export: path + filename to export map (default = '' // no map export)
    :returns: void
    :raises keyError: raises an exception
    """
    
    last_second = int(dataframe.iloc[:,-1].name)
    
    fig, ax = plt.subplots()

    for index, row in dataframe.iterrows():

        start = 0
        ii = 1
        while (ii < last_second+1 and (str(row[ii]) not in locations)):
            start+=1
            ii+=1
        x = []
        y = []
        before = '0'
        for i in range(start+1, last_second+1):
            if (str(row[i]) in locations) and (str(row[i]) != before):
                x.append(locations[str(row[i])][0]*random.uniform(0.98, 1.02)) 
                y.append(locations[str(row[i])][1]*random.uniform(0.98, 1.02))
                before = str(row[i])
        plt.plot(x,y, c = line_color, alpha = 0.3)

    ax.set_xticks([])
    ax.set_yticks([])
    
    img = plt.imread(map_file)
    plt.imshow(img, zorder=0, extent=[0.0, width, 0.0, height], alpha = im_alpha)

    fig.set_figheight(8) 
    fig.set_figwidth(12) 
    
    if export != '':
        fig.savefig(export+".png")
    
    plt.show()


# Space time cube
def space_time_cube(dataframe, p, locations, map_file, width, height, export = ''  ):
    """
    Creates an space time cube for item i in the input dataframe.

    :param dataframe: dataframe with columns providing location information per second
    :param p: position in the dataframe as integer
    :param locations: dict of '<location name>': [x,y]
    :param map_file: path to mapfile for background map
    :param width: width of map in px
    :param height: height of map in px
    :param export: path + filename to export map (default = '' // no map export)
    :returns: void
    :raises keyError: raises an exception
    """
    
    last_second = int(dataframe.iloc[:,-1].name)    
    
    fig, ax = plt.subplots()
    ax = Axes3D(fig)

    for index, row in dataframe[p:p+1].iterrows():

        start = 0
        ii = 1
        while (ii < last_second+1 and (str(row[ii]) not in locations)):
            start+=1
            ii+=1
        x = []
        y = []
        z = []
        before = '0'
        for i in range(start+1, last_second+1):
            if (str(row[i]) in locations) and (str(row[i]) != before):
                x.append(locations[str(row[i])][0]) 
                y.append(locations[str(row[i])][1])
                z.append(i/30)
                before = str(row[i])
        plt.plot(x,y,z, c = '#6848c0')

    ax.axis('auto')

    img = plt.imread(map_file)
    #fig.imshow(img, zorder=0, extent=[0.0, width, 0.0, height], alpha = 0.3)

    #fig.set_figheight(12) 
    #fig.set_figwidth(12)
    
    if export != '':
        fig.savefig(export+".png")

    plt.show()


# Wordcloud
def wordcloud_from_answers(dataframe, column, title, print_count = False, export = ''  ):
    """
    Creates an wordcloud for given column in the input dataframe.

    :param dataframe: dataframe with columns providing location information per second
    :param column: column name in the dataframe as String
    :param title: title of wordcloud
    :param map_file: path to mapfile for background map
    :param print_count: if True, prints word count (default = False)
    :param export: path + filename to export map (default = '' // no map export)
    :returns: void
    :raises keyError: raises an exception
    """    
    def grey_color_func(word, font_size, position, orientation, random_state=None,
                    **kwargs):
        return "hsl(0, 0%, 0%)"
    
    # count words
    words = {}
    for index, row in dataframe.iterrows():
        try:
            for word in dataframe[column][index].split(","):
                if word.strip() in words:
                    words[word.strip()] = words[word.strip()] +1
                else:
                    words[word.strip()] = 1
        except Exception:
            pass
    
    if print_count == True:
        sorted_words = {}
        sorted_keys = sorted(words, key=words.get, reverse=True)

        for w in sorted_keys:
            sorted_words[w] = words[w]
            
        print("Sorted word count for {}: {}".format(title, sorted_words))
    
    # create wordcloud
    wordcloud = WordCloud().generate_from_frequencies(words)

    # lower max_font_size
    wordcloud = WordCloud(background_color="white",max_font_size=80).generate_from_frequencies(words)
    plt.figure()
    plt.title(title, color='red')
    plt.imshow(wordcloud.recolor(color_func=grey_color_func, random_state=3), interpolation="bilinear")
    plt.axis("off")
    
    if export != '':
        plt.savefig(export+".png")
    
    plt.show()


# Multimatch
# https://multimatch.readthedocs.io/en/latest/api.html
def create_paths(dataframe, locations, id_column='id' , folder='', return_pathlist = True):
    """
    Creates scanpath-like data of the input dataframe and exports paths into a tsv-file for multipath anaysis.

    :param dataframe: dataframe with columns providing location information per second
    :param locations: dict of '<location name>': [x,y]
    :param id_column: column name used for filename as String (default = 'id')
    :param folder: folder to save the tsv-files (default = '' // no export)
    :param return_pathlist: if True, returns pathlist (default = False)
    :returns: pathlist if return_pathlist = True
    :raises keyError: raises an exception
    """    
    last_second = int(dataframe.iloc[:,-1].name)  
    path_list = []
    
    for index, row in dataframe.iterrows():
        
        start = 0
        ii = 1
        while (ii < last_second+1 and (str(row[ii]) not in locations)):
            start+=1
            ii+=1
        path = []
        count = 1
        before = '0'
        for i in range(start+1, last_second+1):
            if (str(row[i]) in locations) and (str(row[i]) != before):
                path.append((locations[str(row[i])][0]+random.uniform(-7.5, 7.5), 
                             locations[str(row[i])][1]+random.uniform(-7.5, 7.5), 
                             count))
                before = str(row[i])
                count = 1
            else:
                count +=1
        
        if folder != '':
        
            filename = folder+str(row[id_column])+".tsv"

            with open(filename, 'wt', newline='') as out_file:
                tsv_writer = csv.writer(out_file, delimiter='\t')
                tsv_writer.writerow(("start_x", "start_y", "duration"))
                for p in path:
                    tsv_writer.writerow(p)
        
        path_list.append(path)
    
    if return_pathlist == True:
        return path_list


def create_similarity_measures(dataframe, folder, width, height, id_column ='id'):
    """
    Writes the scanpath-like data of the input dataframe into a tsv-file for multipath anaysis.

    :param dataframe: dataframe with columns providing an id
    :param folder: folder to load the tsv-files
    :param width: width of map in px
    :param height: height of map in px
    :param id_column: column name used for filename as String (default = 'id')
    :returns: dict with 2d arrays of multipath measures for keys "vector", "direction", "length", "position" and "duration"
    """       
    
    indices = dataframe[id_column].to_list()
    
    vectorsim = [[j for j in range(len(indices))] for i in range(len(indices))]
    directionsim = [[j for j in range(len(indices))] for i in range(len(indices))]
    lengthsim = [[j for j in range(len(indices))] for i in range(len(indices))]
    positionsim = [[j for j in range(len(indices))] for i in range(len(indices))]
    durationsim = [[j for j in range(len(indices))] for i in range(len(indices))]
    
    for i in range(len(indices)):
        for j in range(len(indices)):
            fix_vector1 = np.recfromcsv(folder + str(indices[i]) +'.tsv',
                delimiter='\t', dtype={'names': ('start_x', 'start_y', 'duration'),
                'formats': ('f8', 'f8', 'f8')})
            fix_vector2 = np.recfromcsv(folder + str(indices[j]) +'.tsv',
                delimiter='\t', dtype={'names': ('start_x', 'start_y', 'duration'),
                'formats': ('f8', 'f8', 'f8')})
            vectorsim[i][j] = mmg.docomparison(fix_vector1, fix_vector2, screensize=[width, height])[0]
            directionsim[i][j] = mmg.docomparison(fix_vector1, fix_vector2, screensize=[width, height])[1]
            lengthsim[i][j] = mmg.docomparison(fix_vector1, fix_vector2, screensize=[width, height])[2]
            positionsim[i][j] = mmg.docomparison(fix_vector1, fix_vector2, screensize=[width, height])[3]
            durationsim[i][j] = mmg.docomparison(fix_vector1, fix_vector2, screensize=[width, height])[4]
    return {"vector": vectorsim, 
            "direction": directionsim, 
            "length": lengthsim, 
            "position": positionsim, 
            "duration": durationsim}


def k_most_similar_pairs(array, k, dataframe_for_labels, id_column='id'):
    """
    Returns similar k pairs of labels from array
    
    :param array: 2d array of similarity measures
    :param k: number of similar pairs to find as integer
    :param dataframe_for_labels: dataframe with columns providing an id
    :param id_column: column name used for filename as String (default = 'id')
    :returns: most similar k pairs of labels 
    """
    
    labels = dataframe_for_labels[id_column].to_list()
    k_ = k*2+len(dataframe)
    idx = np.argsort(array.ravel())[:-k_-1:-1]
    pairs = np.column_stack(np.unravel_index(idx, array.shape))[len(dataframe):]
    return [(labels[pairs[i][0]], labels[pairs[i][1]]) for i in range(len(pairs))][0::2]


def cluster_similarities(array, threshold_factor):
    """
    Returns cluster of labels from array
    
    :param array: 2d array of similarity measures
    :param threshold_factor: threshold_factor as float between 0 and 1
    :returns: clusters of labels 
    """
    pdist = spc.distance.pdist(array)
    linkage = spc.linkage(pdist, method='complete')
    return spc.fcluster(linkage, threshold_factor * pdist.max(), 'distance')


def create_matrix_from_array(array, dataframe_for_labels, id_column='id', export = ''  ):
    """
    creates similarity matrix from array with labels from dataframe
    
    :param array: 2d array of similarity measures
    :param dataframe_for_labels: dataframe with columns providing an id
    :param id_column: column name used for filename as String (default = 'id')
    :param export: path + filename to export map (default = '' // no map export)
    :returns: void
    """
    
    x = np.array(array)
    
    labels = dataframe_for_labels[id_column].to_list()

    fig, ax = plt.subplots()
    im = ax.imshow(x,cmap='YlGnBu')

    ax.set_xticks(np.arange(len(labels)))
    ax.set_yticks(np.arange(len(labels)))

    ax.set_xticklabels(labels)
    ax.set_yticklabels(labels)

    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    for i in range(len(labels)):
        for j in range(len(labels)):
            text = ax.text(j, i, x[i, j].round(2),
                           ha="center", va="center", color="w")

    fig.tight_layout()
    fig.set_figheight(20) 
    fig.set_figwidth(20)
    
    if export != '':
        fig.savefig(export+".png")
    
    plt.show()


def print_similarity_cluster(measures, measure, threshold_factor, dataframe_for_labels, id_column='id'):
    """
    Prints cluster of labels from array
    
    :param measures: dict with 2d arrays of multipath measures for keys "vector", "direction", "length", "position" and "duration"
    :param measure: measure as String in {"vector", "direction", "length", "position", "duration"}
    :param threshold_factor: threshold_factor as float between 0 and 1
    :param dataframe_for_labels: dataframe with columns providing an id
    :param id_column: column name used for filename as String (default = 'id')
    :returns: void
    """
    
    labels = dataframe_for_labels[id_column].to_list()
    ms = np.array(measures[measure])
    c = cluster_similarities(ms, threshold_factor)

    for i in range (1, max(c)):
        print("Cluster {}:".format(i))
        for j in range(len(c)):
            if c[j] == i:
                print(labels[j])


# Mean path length 
# Grundlage: path_to_file()
def path_lengths_viz(paths, dataframe_for_labels, id_column='id', sort = True, export = ''):
    """
    Visualizes sorted path lengths for for every row of the dataframe.
    
    :param paths: pathlist returned from paths_to_file function
    :param dataframe_for_labels: dataframe with columns providing an id
    :param id_column: column name used for filename as String (default = 'id')
    :param sort: if True, sort by length (default = True)
    :param export: path + filename to export map (default = '' // no map export)
    :returns: void
    """
    
    labels = dataframe_for_labels[id_column].to_list()
    
    lengths = []
    for path in paths:
        lengs = []
        for i in range(1, len(path)):
            lengs.append(math.hypot(path[i-1][0] - path[i][0], path[i-1][1] - path[i][1]))
        lengths.append(lengs)
    
    l = len(lengths)
    
    fig_columns = 3
    if int(l%3) == 0: 
        fig_rows = int(l/3)
    else:
        fig_rows = int(l/3)+1
    
    fig, axs = plt.subplots(fig_rows, fig_columns, constrained_layout=True)
    indec = 0    

    for p in lengths:
        rown = int(int(indec)/3)
        coln = (int(indec)%3)
        
        if sort == True:
            lp = np.asarray(np.sort(p))
        else:
            lp = np.asarray(p)
        y_pos = np.arange(len(lp))

        if fig_rows == 1:
            axs[coln].barh(y_pos, lp, align='center')
            axs[coln].set_yticks(y_pos)
            axs[coln].set_xlim([0, 1200])
            axs[coln].invert_yaxis()  # labels read top-to-bottom
            axs[coln].vlines([np.mean(lp)], -1, len(lp), colors='r', linewidths = 5.)
            axs[coln].set_yticks([])
            axs[coln].text(900.0, 0, labels[indec], fontsize=40, verticalalignment='top')
        
        else:
            axs[rown, coln].barh(y_pos, lp, align='center')
            axs[rown, coln].set_yticks(y_pos)
            axs[rown, coln].set_xlim([0, 1200])
            axs[rown, coln].invert_yaxis()  # labels read top-to-bottom
            axs[rown, coln].vlines([np.mean(lp)], -1, len(lp), colors='r', linewidths = 5.)
            axs[rown, coln].set_yticks([])
            axs[rown, coln].text(900.0, 0, labels[indec], fontsize=40, verticalalignment='top')

        indec +=1
    
    while indec < fig_rows * fig_columns:
        rown = int(int(indec)/3)
        coln = (int(indec)%3)
        axs[rown, coln].set_yticks([])
        axs[rown, coln].set_xlim([0, 1200])
        indec +=1
    
    fig.set_figheight(fig_rows*10) 
    fig.set_figwidth(40)
        
    if export != '':
        fig.savefig(export+".png")
    
    plt.show()
    
    
def most_frequent_sequences(dataframe, locations, min_o, export = ''):
    """
    Returns sequences from dataframe with at least min_o occurances
    
    :param dataframe: dataframe with columns providing location information per second
    :param locations: dict of '<location name>': [x,y]
    :param min_o: number of minimum occurances to find as integer
    :param export: path + filename to export map (default = '' // no map export)
    :returns: void
    """
    
    # defining variables from input
    last_second = int(dataframe.iloc[:,-1].name)
    
    cluster = {}
    for key in locations:
        cluster[key] = 0
    
    cluster_flow = {}
    for key in cluster:
        cluster_flow[key] = copy.deepcopy(cluster)
    
    sequences = {}
    for key in cluster:
        sequences[key] = copy.deepcopy(cluster_flow)
        
    
    for index, row in dataframe.iterrows():
    
        before = '0'
        before2 = '0'
            
        for i in range(2, last_second):
                
            if (str(row[i]) in locations):
                if (str(row[i]) != before):
                    if before != before2 and (before2 != '0'):
                        sequences[before2][before][str(row[i])] = sequences[before2][before][str(row[i])] +1
                    before2 = before
                    before = str(row[i])
                else:
                    before = str(row[i])
    
    sequ = {}
    for k in sequences.keys():
        for l in sequences[k].keys():
            for m in sequences[k][l].keys():
                if sequences[k][l][m] >= min_o:
                    sequ[str(k+"-"+l+"-"+m)] = sequences[k][l][m]
    
    sorted_sequences = {}
    sorted_keys = sorted(sequ, key=sequ.get, reverse = True)

    for w in sorted_keys:
        sorted_sequences[w] = sequ[w]
    
    
    fig, ax = plt.subplots()

    paths = list(sorted_sequences.keys())
    y_pos = np.arange(len(paths))
    lengths = sorted_sequences.values()

    ax.barh(y_pos, lengths, align='center')

    ax.set_yticks(y_pos)
    ax.set_yticklabels(paths)
    ax.invert_yaxis()  # labels read top-to-bottom
    ax.invert_xaxis()
    ax.set_xlabel('N occurances')
    ax.set_title('Most frequent subsequences')
    
    if export != '':
        fig.savefig(export+".png")

    plt.show()

    