# Think aloud visual analytics

During an ongoing pandemic, execution of eye-tracking user studies in indoor laboratories is not possible. We introduce an alternative method to eye-tracking by combining Think Aloud interviews with techniques from Visual Analytics.

In our user study, we ask participants to:
-	Fulfil a personal questionnaire
-	Perform test for the cognitive style via [PsyToolkit](https://www.psytoolkit.org/)
-	Take part in a think-aloud interview where participants have to solve five high-level interpretation tasks and describe their reasoning
and recorded the interviews via Zoom.

<img src="images/ta_study_design.png" alt="study design" width="600"/>

## Encoding

We introduce a 3-dimensional encoding system for the visual analysis of the interviews. We defined seven content categories before encoding the interviews:(1) task description, (2) interviewer’s question, (3) participant’s question, (4) non-task-related discussion, (5) legend, (6) background map and (7) data referring either to the whole dataset or one of the attributes/areas encoded as subclasses of (7).


## Visualizations
From the results of the encoding process, we created visualizations using techniques from eye tracking analysis ([Andrienko et al. 2012](https://doi.org/10.1109/TVCG.2012.276)).

### Attention maps
<img src="images/attention_map1.png" alt="attention map" height="190"/> &nbsp;
<img src="images/attention_map2.png" alt="attention map" height="200"/>

### Flow maps
<img src="images/flow_map1.png" alt="flow map" height="200"/> &nbsp;
<img src="images/flow_map2.png" alt="flow map" height="190"/>

### Trajectory paths & path lengths
<img src="images/trajectories.png" alt="attention map" height="300"/> &nbsp;
<img src="images/trajectory_len.png" alt="attention map" height="300"/>


### Temporal view of trajectories
<img src="images/temporal_view1.png" alt="temporal view" height="200"/>


## Encoding example
We provide an encoding example using MS Ecxel. Four every second (green box), we mark the category the user refers to (blue box) and the predefined location on the map (red box). The yellow box shows a combination of the encoded category (last digit, e.g. blue = "3") and the location (e.g. location "12").

<img src="images/encoding.png" alt="encoding example"/>

The whole file is analysed using the think aloud visualization library as shown in the example.


## Publications
Knura, M., & Schiewe, J. (2021). Map Evaluation under COVID-19 restrictions: A new visual approach based on think aloud interviews. Proceedings of the ICA, 4, 60. [DOI: 10.5194/ica-proc-4-60-2021](https://www.proc-int-cartogr-assoc.net/4/60/2021/)

